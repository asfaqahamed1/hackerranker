package main

import "fmt"

func main() {
	a := []int32{0, 0, 1, 2, 1}
	ints := lonelyinteger(a)
	fmt.Print(ints)
}

type set map[int32]int32

func (s set) sum() int32 {
	var total_value int32
	for _, v := range s {
		total_value += v
	}

	return total_value
}
func sumAllNumArray(sli []int32) int32 {
	var sum int32
	for _, value := range sli {
		sum += value
	}
	return sum
}

func createSet(a []int32) set {
	set_list := make(set)
	for _, value := range a {
		_, existis := set_list[value]
		if !existis {
			set_list[value] = value
		}
	}
	return set_list
}
func lonelyinteger(a []int32) int32 {
	set_list := createSet(a)
	lonely_int_total := set_list.sum()
	total_sum := sumAllNumArray(a)
	fmt.Println(lonely_int_total, total_sum)
	return (lonely_int_total * 2) - total_sum
}
