def lonelyinteger(a):
    # Write your code here
    lonely_int_sum = sum(set(a))
    total_sum = 0
    for i in range(len(a)):
        total_sum += a[i]    
    return (lonely_int_sum *2) - total_sum

lonelyinteger([1])