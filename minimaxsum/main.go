package main

import (
	"fmt"
	"sort"
)

func main() {
	sli := []int32{396285104, 573261094, 759641832, 819230764, 364801279}
	miniMaxSum(sli)

}
func sumAllNumArray(sli []int32) int {
	var sum int
	for _, value := range sli {
		sum += int(value)
	}
	return sum
}

func miniMaxSum(sli []int32) {
	var sumSlice []int
	total_sum := sumAllNumArray(sli)

	for i := 0; i < len(sli); i++ {
		current_total_sum := total_sum - int(sli[i])
		sumSlice = append(sumSlice, current_total_sum)
	}
	fmt.Println(sumSlice)
	sort.Slice(sumSlice, func(i, j int) bool { return sumSlice[i] < sumSlice[j] })
	fmt.Printf("%d %d", sumSlice[0], sumSlice[len(sumSlice)-1])
}
