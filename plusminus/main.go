package main

import (
	"fmt"
)

func main() {
	sli := []int32{1, 1, 0, -1, -1}
	plusMinus(sli)
}

func plusMinus(arr []int32) {
	list_length := float32(len(arr))
	var neagative_count, positive_count, zero_count float32

	for _, interger := range arr {
		if interger == 0 {
			zero_count++
		} else if interger > 0 {
			positive_count++
		} else if interger < 0 {
			neagative_count++
		} else {
			fmt.Print("something unusual has encountred")
		}
	}
	neagative_count_ratio := neagative_count / list_length
	positive_count_ratio := positive_count / list_length
	zero_count_ratio := zero_count / list_length
	fmt.Printf("%.6f\n%.6f\n%.6f\n", neagative_count_ratio, positive_count_ratio, zero_count_ratio)

}
