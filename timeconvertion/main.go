package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	timeConversion("12:45:54PM")
}

func timeConversion(s string) string {
	spiltedDate := strings.Split(s, ":")
	hr := spiltedDate[0]
	min := spiltedDate[1]
	sec := spiltedDate[2][0:2]
	period := spiltedDate[2][2:]
	military_hr, _ := strconv.Atoi(hr)
	if period == "PM" {
		military_hr = (military_hr + 12) % 24

	} else {
		military_hr = military_hr % 12
		fmt.Print(military_hr)
	}
	var mil_date_string string
	if military_hr > 10 {
		mil_date_string = fmt.Sprint(military_hr)
	} else {
		mil_date_string = "0" + fmt.Sprint(military_hr)
	}
	mil_date_string = fmt.Sprintf("%s:%s:%s", mil_date_string, min, sec)
	return mil_date_string
}
